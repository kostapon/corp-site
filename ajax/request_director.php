<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>
<?$APPLICATION->IncludeComponent(
	"axi:main.feedback", 
	"request_director", 
	array(
		"EMAIL_TO" => "",
		"EVENT_MESSAGE_ID" => array(
                        0=>"11"
		),
		"OK_TEXT" => "Спасибо, ваше сообщение принято. Мы с вами обязательно свяжемся в ближайшее время.",
		"REQUIRED_FIELDS" => array(
                     0=>"PHONE"
		),
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => "request_director",
        "INFOBLOCK_ID" => "16",
	),
	false
);?>
<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>