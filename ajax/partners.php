<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>
<?$APPLICATION->IncludeComponent(
	"axi:main.feedback", 
	"ajax_partners", 
	array(
		"EMAIL_TO" => "",
		"EVENT_MESSAGE_ID" => array(
                        0=>"10"
		),
		"OK_TEXT" => "Спасибо, ваше сообщение принято. Мы с вами обязательно свяжемся в ближайшее время.",
		"REQUIRED_FIELDS" => array(
                     0=>"PHONE"
		),
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => "ajax_partners",
        "INFOBLOCK_ID" => "15",
	),
	false
);?>
<?
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>