<div class="about min-width1240">
    <div class="about-content">
        <h2 class="about-header">О нас</h2>
        <div class="about-text">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:main.include",
            	".default",
            	Array(
            		"AREA_FILE_SHOW" => "file",
            		"AREA_FILE_SUFFIX" => "inc",
            		"COMPONENT_TEMPLATE" => ".default",
            		"EDIT_TEMPLATE" => "",
            		"PATH" => SITE_DIR."/include/about_text.php"
            	)
            );?>
        </div>
    </div>
</div>