<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt=$APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
	"IS_SEF" => "Y",
	"SECTION_PAGE_URL" => "#SECTION_CODE_PATH#/",
	"DETAIL_PAGE_URL" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#",
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "5",
	"DEPTH_LEVEL" => "4",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000"
	),
	false
);

$url=GetPagePath();
$url=explode("/",$url);
$url="/".$url[1];

$aMenuLinks=Array( Array(
		$APPLICATION->GetTitle(), 
		$url, 
		Array(), 
		Array(), 
		"" 
	)
    );
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>