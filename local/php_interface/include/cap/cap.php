<?//ОПРЕДЕЛНИЕ БРАУЗЕРА И ПЕРЕХОД НА ЗАГЛУШКУ ЕСЛИ НУЖНО
function fetch_browser($s = '')
    {
        $version = 0;
        $browser = "unknown";

        $useragent = $s ? $s : $_SERVER['HTTP_USER_AGENT'];
        $useragent = strtolower($useragent);


        //-----------------------------------------
        // Opera...
        //-----------------------------------------

        if (strstr($useragent, 'opera'))
        {
            preg_match("#opera[ /]([0-9\.]{1,10})#", $useragent, $ver);

            return array('browser' => 'opera', 'version' => $ver[1], 'name' => 'Opera ' . $ver[1]);
        }

        //-----------------------------------------
        // IE...
        //-----------------------------------------

        if (strstr($useragent, 'msie'))
        {
            if (stripos($useragent, 'MSIE 6.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 7.0') === false)
            {
                header("Location: /ie67/ie6.html");
            }
            if (stripos($useragent, 'MSIE 7.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 9.0') === false)
            {
                header("Location: /ie67/ie7.html");
            }
            if (stripos($useragent, 'MSIE 8.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 9.0') === false)
            {
                header("Location: /ie67/ie8.html");
            }
            if (stripos($useragent, 'MSIE 9.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 9.0') === false)
            {
                header("Location: /ie67/ie9.html");
            }
            preg_match("#msie[ /]([0-9\.]{1,10})#", $useragent, $ver);
            return array('browser' => 'ie', 'version' => $ver[1], 'name' => 'IE ' . $ver[1]);
        }

        //-----------------------------------------
        // Chrome...
        //-----------------------------------------

        if (strstr($useragent, 'chrome'))
        {
            preg_match("#chrome/([0-9.]{1,10})#", $useragent, $ver);

            return array('browser' => 'chrome', 'version' => $ver[1], 'name' => 'Chrome ' . $ver[1]);
        }

        //-----------------------------------------
        // Safari...
        //-----------------------------------------

        if (strstr($useragent, 'safari'))
        {
            preg_match("#safari/([0-9.]{1,10})#", $useragent, $ver);

            return array('browser' => 'safari', 'version' => $ver[1], 'name' => 'Safari ' . $ver[1]);
        }

        //-----------------------------------------
        // FireFox
        //-----------------------------------------

        if (strstr($useragent, 'firefox'))
        {
            preg_match("#firefox[ /]([0-9\.]{1,10})#", $useragent, $ver);

            return array('browser' => 'firefox', 'version' => $ver[1], 'name' => 'FireFox ' . $ver[1]);
        }

        //-----------------------------------------
        // Mozilla browsers...
        //-----------------------------------------

        if (strstr($useragent, 'gecko'))
        {
            preg_match("#gecko/(\d+)#", $useragent, $ver);

            return array('browser' => 'gecko', 'version' => $ver[1], 'name' => 'Gecko Engine ' . $ver[1]);
        }

        //-----------------------------------------
        // Older Mozilla browsers...
        //-----------------------------------------

        if (strstr($useragent, 'mozilla'))
        {
            preg_match("#^mozilla/[5-9]\.[0-9.]{1,10}.+rv:([0-9a-z.+]{1,10})#", $useragent, $ver);

            return array('browser' => 'mozilla', 'version' => $ver[1], 'Mozilla ' . $ver[1]);
        }

        //-----------------------------------------
        // Konqueror...
        //-----------------------------------------

        if (strstr($useragent, 'konqueror'))
        {
            preg_match("#konqueror/([0-9.]{1,10})#", $useragent, $ver);

            return array('browser' => 'konqueror', 'version' => $ver[1], 'name' => 'Konqueror ' . $ver[1]);
        }

        //-----------------------------------------
        // Still here?
        //-----------------------------------------

        return array('browser' => $browser, 'version' => $version, 'name' => 'Unknown');
    }
    
    fetch_browser();
?>