﻿<?
include 'include/cap/cap.php';

class Geo {

    var $ip;
    var $charset = 'utf-8';

    public function __construct($options = null) {

        $this->dirname = dirname(__file__);

        // ip
        if (isset($options['ip']) && $this->is_valid_ip($options['ip'])) {
            $this->ip = $options['ip'];
        } else {
            $this->ip = $this->get_ip();
        }
        // кодировка
        if (isset($options['charset']) && is_string($options['charset']) && $options['charset'] != 'windows-1251') {
            $this->charset = $options['charset'];
        }
    }

    /**
     * функция возвращет конкретное значение из полученного массива данных по ip
     * @param string - ключ массива. Если интересует конкретное значение. 
     * Ключ может быть равным 'inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng'
     * @param bolean - устанавливаем хранить данные в куки или нет
     * Если true, то в куки будут записаны данные по ip и повторные запросы на ipgeobase происходить не будут.
     * Если false, то данные постоянно будут запрашиваться с ipgeobase
     * @return array OR string - дополнительно читайте комментарии внутри функции.
     */
    function get_value($key = null, $cookie = true) {
        $key_array = array('inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng');
        if (!in_array($key, $key_array)) {
            $key = null;
        }
        $data = $this->get_data($cookie);
        if ($key) { // если указан ключ 
            if (isset($data[$key])) { // и значение есть в массиве данных
                return $data[$key]; // возвращаем строку с нужными данными
            } elseif ($cookie) { // иначе если были включены куки
                return $this->get_value($key, false); // пытаемся вернуть данные без использования cookie
            }
            return NULL; // если ничего нет - отдаем NULL
        }
        return $data; // иначе возвращаем массив со всеми данными            
    }

    /**
     * Получаем данные с сервера или из cookie
     * @param boolean $cookie
     * @return string|array
     */
    function get_data($cookie = true) {
        // если используем куки и параметр уже получен, то достаем и возвращаем данные из куки
        if ($cookie && filter_input(INPUT_COOKIE, 'geobase')) {
            return unserialize(filter_input(INPUT_COOKIE, 'geobase'));
        }
        $data = $this->get_geobase_data();
        if (!empty($data)) {
            setcookie('geobase', serialize($data), time() + 3600 * 24 * 7, '/'); //устанавливаем куки на неделю
        }
        return $data;
    }

    /**
     * функция получает данные по ip.
     * @return array - возвращает массив с данными
     */
    function get_geobase_data() {
        // получаем данные по ip
        $opts = array(
          'http' => array(
            'method'  => 'GET'
          )
        );

        $context = stream_context_create($opts);
        $address = 'http://ipgeobase.ru:7020/geo?ip=' . $this->ip;
        $string = file_get_contents($address, false, $context);
        
        $data = $this->parse_string($string);
        return $data;
    }

    /**
     * функция парсит полученные в XML данные в случае, если на сервере не установлено расширение Simplexml
     * @return array - возвращает массив с данными
     */
    function parse_string($string) {
        $params = array('inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng');
        $data = $out = array();
        foreach ($params as $param) {
            if (preg_match('#<' . $param . '>(.*)</' . $param . '>#is', $string, $out)) {
                $data[$param] = trim($out[1]);
            }
        }
        return $data;
    }

    /**
     * функция определяет ip адрес по глобальному массиву $_SERVER
     * ip адреса проверяются начиная с приоритетного, для определения возможного использования прокси
     * @return ip-адрес
     */
    function get_ip() {
        $keyname_ip_arr = array('HTTP_X_FORWARDED_FOR','HTTP_X_FORWARDED','HTTP_REMOTE_ADDR_REAL','HTTP_CLIENT_IP','HTTP_X_REAL_IP','HTTP_FORWARDED_FOR','HTTP_FORWARDED','HTTP_X_COMING_FROM','HTTP_COMING_FROM','HTTP_X_CLUSTER_CLIENT_IP','REMOTE_ADDR');
        foreach ($keyname_ip_arr as $keyname_ip) {
          if (!empty($_SERVER[$keyname_ip])) {
            $ip = trim($_SERVER[$keyname_ip]);
            if (strstr($ip, ',')) {
               $ips = explode(',', $ip);
               foreach ($ips as $ip) {
                 if (filter_var(trim($ip), FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE)) break(2);
               }
            }
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE)) break;
          }
        }
        return trim($ip);
    }

    /**
     * функция для проверки валидности ip адреса
     * @param ip адрес в формате 1.2.3.4
     * @return bolean : true - если ip валидный, иначе false
     */
    function is_valid_ip($ip = null) {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return true;
        }
        return false; // иначе возвращаем false
    }

}

/**
 * SEND SMTP
 */
require_once 'include/swiftmailer/swiftmailer/swift_required.php';
 
function custom_mail($to, $subject, $msg, $additionalHeaders)
{
     if(strpos($additionalHeaders, "text/plain")){
        $text_type = "text/plain"; 
     }
     else{
        $text_type = "text/html";
     }
    $toList = explode(',',$to);
     
    Swift_Preferences::getInstance()->setCharset('utf-8');
     
    $transport = Swift_SmtpTransport::newInstance('')
        ->setPort(465)
        ->setEncryption('ssl')
        ->setUsername('')
        ->setPassword('')
    ;
     
    $mailer = Swift_Mailer::newInstance($transport);
     
    $message = Swift_Message::newInstance($subject)
        ->setFrom(array('' => 'no-reply'))
        ->setTo($toList)
        ->setBody($msg,$text_type)
    ;
     
    $result = $mailer->send($message); 
  
}

?>