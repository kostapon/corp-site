<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/fonts/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/fancybox/jquery.fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/scrollbar/jquery.mCustomScrollbar.min.css" />
    
    <meta name="google-site-verification" content="hxfcFi2QcvxB59UPXZszpu938IVW989nwLcxJPft7UI" />
    <meta name="yandex-verification" content="a25f2375cabc8422" />
    
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery/jquery-1.12.4.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery/jquery-ui.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery/js.cookie.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick/slick.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/scrollbar/jquery.mCustomScrollbar.concat.min.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/inputmask/jquery.inputmask.bundle.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/inputmask/phone-codes/phone.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/inputmask/phone-codes/phone-be.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/inputmask/phone-codes/phone-ru.js');?>
    <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/scripts.js');?>
    
    <?$APPLICATION->ShowHead();?>
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
    <title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>
<?
if ($APPLICATION->GetCurPage() == SITE_DIR . "index.php" || $APPLICATION->GetCurDir() == SITE_DIR) {
    $index = true;
} else {
    $no_index = true;
}
?>
<? $APPLICATION->ShowPanel(); ?>
<div class="body-conteiner min-width1240">
<div class="header min-width1240">
    <div class="header-top <?if($index){echo 'width1240';}else{ if($GLOBALS['width1024']){echo 'width1240';}else{echo 'width1015';}}?>">
        <a href="<?=SITE_DIR?>" class="header-logo">
            <?if($index){?><h1><?}?>
                <?$APPLICATION->IncludeComponent(
                	"bitrix:main.include",
                	".default",
                	Array(
                		"AREA_FILE_SHOW" => "file",
                		"AREA_FILE_SUFFIX" => "inc",
                		"COMPONENT_TEMPLATE" => ".default",
                		"EDIT_TEMPLATE" => "",
                		"PATH" => SITE_DIR."/include/logo.php"
                	)
                );?>
            <?if($index){?></h1><?}?>
        </a>
        <div class="header-contactblock <?if($index){echo 'width685';}else{ if($GLOBALS['width1024']){echo 'width685';}else{echo 'width600';}}?>">
            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"adress",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "addresses",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"ADRESS",1=>"VALUE_PHONE",2=>"PHONE",3=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
        </div>
    </div>
    <div class="header-3dslider-1urov">
        <div class="header-3dslider-2urov">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:menu",
            	"horizontal_multilevel1",
            	Array(
            		"ALLOW_MULTI_SELECT" => "N",
            		"CHILD_MENU_TYPE" => "left",
            		"DELAY" => "N",
            		"MAX_LEVEL" => "2",
            		"MENU_CACHE_GET_VARS" => array(0=>"",),
            		"MENU_CACHE_TIME" => "3600",
            		"MENU_CACHE_TYPE" => "N",
            		"MENU_CACHE_USE_GROUPS" => "Y",
            		"ROOT_MENU_TYPE" => "top",
            		"USE_EXT" => "Y"
            	)
            );?>
            <?$APPLICATION->IncludeComponent(
            	"bitrix:search.form",
            	"search",
            	Array(
            		"PAGE" => "#SITE_DIR#search/index.php",
            		"USE_SUGGEST" => "N"
            	)
            );?>
        </div>
    </div>
    
</div>
<!--CONTENT-->
<?if($no_index){?>
    <div class="textpage min-width1240"><div class="content <?if($index){echo 'width1240';}else{ if($GLOBALS['width1024']){echo 'width1240';}else{echo 'width1015';}}?>">
    
    <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "template1", Array(
    	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
    		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
    		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
    		"COMPONENT_TEMPLATE" => ".default"
    	),
    	false
    );?>
    
    <h1 class="content-header"><?$APPLICATION->ShowTitle(false)?></h1>
<?}?>