/**
* < C O N T A C T B L O C K >
*/
$(document).ready(function(){ 
    $('.contactblock-sity').click(function(){
        if($(this).parent().find('.contactblock-sities').is('.active')){
            $(this).parent().find('.contactblock-sities').removeClass('active');
        }else{
            $(this).parent().find('.contactblock-sities').addClass('active');
        }
        $(this).parent().find('.contactblock-sities').toggle('fade');
    });
    $('.contactblock-sities li').click(function(){
        var data_adres=$(this).attr('data-adres');
        var data_phone=$(this).attr('data-phone');
        var data_phone_value=$(this).attr('data-phone-value');
        var sity=$(this).html();
        $('.contactblock-sity').html(sity);
        $('.contactblock-adres').html(data_adres);
        $('.phone-sity').html(data_phone);
        $('.phone-sity').attr('href','tel:'+data_phone_value);
        $(this).parent().toggle('fade');
        sity=$(this).find('span').html();
        Cookies.set('BITRIX_SM_SITY', sity, { expires: 365, path: '/' });
       
    });
});
//</ C O N T A C T B L O C K >

/**
* < B U T T O M   S E A R C H   S L I D E >
*/
$(document).ready(function(){
    if(navigator.appName!='Microsoft Internet Explorer' && navigator.appVersion.indexOf('Trident')==-1 ){
        $('.header-searchbuttom').click(function(){
            $('.search-text').focus();
            var clickflag=false;
            $('.header-3dslider-2urov').css('transform','rotateX(90deg)');
            $('.header-3dslider-2urov').mouseout(function(){
                clickflag=true;
            });
            $('.header-3dslider-2urov').mouseover(function(){
                clickflag=false;
            });
            $(document).click(function(){
                if(clickflag==true){
                    $('.header-3dslider-2urov').css('transform','rotateX(0deg)');
                }
            });
        });
    }else{
        $('.header-3dslider-2urov .header-search').hide().css('transform','none');
        var clickflag=false;
        var clickbut=false;
        $('.header-searchbuttom').click(function(){
            $('.search-text').focus();
            clickbut=true;
            $('.header-3dslider-2urov .header-search').toggle();
            $('.header-3dslider-2urov .header-menu').toggle();
            $('.header-3dslider-2urov').mouseout(function(){
                if(clickbut==true)
                    clickflag=true;
            });
            $('.header-3dslider-2urov').mouseover(function(){
                if(clickbut==true)
                    clickflag=false;
            });
            $(document).click(function(){
                if(clickflag==true){
                    $('.header-3dslider-2urov .header-search').toggle();
                    $('.header-3dslider-2urov .header-menu').toggle();
                    clickflag=false;
                    clickbut=false;
                }
            });
        });
    }
});
//</ B U T T O M   S E A R C H   S L I D E >

/**
* < M A I N  S L I D E R >
*/
$(document).ready(function(){
    $('.slider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000
    });
    $('.down-mouse').click(function(){
        $('body,html').animate({scrollTop:928},800);
    });
});
//</ M A I N  S L I D E R >

/**
* < S H O W  M A T E R I A L S >
*/
$(document).ready(function(){
    if(navigator.appName!='Microsoft Internet Explorer'){
        $(window).scroll(function() {
            if($('.materials-element').is('a')){
                var object=$('.materials-element').eq(0);
                var r=object.offset().top-$(this).scrollTop();
                if( r + object.height() <= window.innerHeight) {
                    $('.materials-element').addClass('materials-element-active');
                    
                    function acl(){
                        $('.materials-element').addClass('materials-element-activemouse');
                    }
                    
                    setTimeout(acl, 1000);
                    
                } 
            }
        });
    }else{
        $('.materials-element').addClass('materials-element-active');
    }
});
//</ S H O W  M A T E R I A L S >

/**
* < S L I D E R  P A R N T E R S >
*/
$(document).ready(function(){
    $('.partners-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000
    });
});
//</ S L I D E R  P A R N T E R S >

/**
* < B U T T O M  U P >
*/
$(document).ready(function(){
    $(window).scroll(function() {
        if($(this).scrollTop() >= 245) {
            $('#vverh').fadeIn();
        } else {
            $('#vverh').fadeOut();
        } 
    });
 
    $('#vverh').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });
});
//</ B U T T O M  U P >

/**
* < C A T A L O G  L I S T  S H O W >
*/
$(document).ready(function(){
    $('.catalog-list li .arrow').click(function(){
        if(!$(this).is('.catalog-list  li li .arrow')){
            if(!$(this).parent().parent().is('.activshow')){
                $('.activshow').removeClass('activshow');
                $(this).parent().parent().addClass('activshow');
            }else{
                $(this).parent().parent().removeClass('activshow');
            }
        }
    });
});
//< C A T A L O G  L I S T >

/**
* < C A T A L O G  E L E M E N T  S L I D E R >
*/
$(document).ready(function(){
    $('.elementslider').slick({
        autoplay: true,
        autoplaySpeed: 5000
    });
    $('.elementslider a').click(function(){
        $('.elementslider').slick('slickPause');
    });
});
//</ C A T A L O G  E L E M E N T  S L I D E R >

/**
* < P E R E C L U C H A T E L  C A R T O C H K I >
*/
$(document).ready(function(){
    $('.elementdes-header li').click(function(){
        if(!$(this).is('.active')){
            var data_id=$('.elementdes-header li.active').attr('data-id');
            var data_id_act=$(this).attr('data-id');
            $('.elementdes-header li.active').removeClass('active');
            $('.elementdes-header li[data-id="'+data_id_act+'"]').addClass('active');
            $('.elementdes-telo div[data-block="'+data_id+'"]').animate({opacity:0},200,function(){
                $('.elementdes-telo div[data-block="'+data_id+'"]').hide();
                $('.elementdes-telo div[data-block="'+data_id_act+'"]').show();
                $('.elementdes-telo div[data-block="'+data_id_act+'"]').animate({opacity: 1}, 200, function(){
                });
            });
            
            
        }
    });
});
//</ P E R E C L U C H A T E L  C A R T O C H K I >

/**
* < S L I D E R  P A R T N E R S >
*/
$(document).ready(function(){
    $('.sliderpartners').slick({
        autoplay: true,
        autoplaySpeed: 5000
    });
});
//</ S L I D E R  P A R T N E R S >

/**
* < M A S K I N P U T >
*/
$(document).ready(function(){
    $('.maskinput').inputmask('+7(999) 999-99-99');
});
//</ M A S K I N P U T >

/**
* < S H O W  M E N U >
*/
$(document).ready(function(){
    var secondmenu=false;
    $('.header-menu ul li').mouseover(function(){
        secondmenu=true;
        $(this).find('ul').eq(0).clone()
                                .addClass('secondmenu')
                                .appendTo('.header')
                                .offset({top: $(this).offset().top+63, left: $(this).offset().left})
                                .mouseleave(function(){
                                    secondmenu=false;
                                    secondmenu_del();
                                })
                                .mouseover(function(){
                                    secondmenu=true;
                                })
                                .mCustomScrollbar();
        $(this).mouseleave(function(e){
            secondmenu=false;
            setTimeout(secondmenu_del,1);
        });
    });
    function secondmenu_del(){
        var count=$('.secondmenu').length;
        if(secondmenu==false){
            count++;
        }
        var i=0;
        while(i<count-1){
            $('.secondmenu').eq(i).remove();
            i++;
        }
    }
});
//</ S H O W  M E N U >

/**
* < C A L L B A C K  O R D E R >
*/
$(document).ready(function(){
    $('.contactblock-collback, .footer-order, .partners-start, .requisites-callback, #call-back').fancybox({
        fullScreen : false,
        closeClickOutside : true,
        baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
			'<div class="fancybox-bg"></div>' +
			'<div class="fancybox-controls callback-close">' +
				'<div class="fancybox-buttons">' +
					'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
				'</div>' +
			'</div>' +
			'<div class="fancybox-slider-wrap">' +
				'<div class="fancybox-slider callback">'+
                '</div>' +
			'</div>' +
            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
		'</div>',
    });
    
});
//</ C A L L B A C K  O R D E R >

/**
* < C A L L B A C K  O R D E R  P R O D U Y C T S  A N D  M A T E R I A L S >
*/
$(document).ready(function(){
    $('.elemenparam-buy').fancybox({
        fullScreen : false,
        closeClickOutside : true,
        baseTpl	: '<div class="fancybox-container fancybox-container-height720" role="dialog" tabindex="-1">' +
			'<div class="fancybox-bg"></div>' +
			'<div class="fancybox-controls callback-close">' +
				'<div class="fancybox-buttons">' +
					'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
				'</div>' +
			'</div>' +
			'<div class="fancybox-slider-wrap">' +
				'<div class="fancybox-slider callback">'+
                '</div>' +
			'</div>' +
            '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
		'</div>',
    });
    
});
//</ C A L L B A C K  O R D E R  P R O D U Y C T S  A N D  M A T E R I A L S >

$(document).ready(function(){
    $('.img-fansibox, a[data-fancybox="elementslider"]').fancybox({
    	thumbs     : false,
        baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
			'<div class="fancybox-bg"></div>' +
                '<div class="fancybox-infobar">'+
                    '<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
                '</div>'+
                '<div class="fancybox-infobar">'+
			         '<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +
                '</div>'+
			'<div class="fancybox-controls">' +
				'<div class="fancybox-buttons">' +
					'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
				'</div>' +
			'</div>' +
			'<div class="fancybox-slider-wrap">' +
				'<div class="fancybox-slider"></div>' +
			'</div>' +
			'<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
		'</div>',
    });
});

