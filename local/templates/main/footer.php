<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<?
global $no_index;
?>
<?if($no_index){
    echo '</div></div>';
}?>
<!--/CONTENT-->
<div class="footer min-width1240">
    <div class="footer-content">
                <?$APPLICATION->IncludeComponent(
            	"bitrix:menu",
            	"footer_menu",
            	Array(
            		"ALLOW_MULTI_SELECT" => "N",
            		"CHILD_MENU_TYPE" => "left",
            		"DELAY" => "N",
            		"MAX_LEVEL" => "2",
            		"MENU_CACHE_GET_VARS" => array(0=>"",),
            		"MENU_CACHE_TIME" => "3600",
            		"MENU_CACHE_TYPE" => "N",
            		"MENU_CACHE_USE_GROUPS" => "Y",
            		"ROOT_MENU_TYPE" => "top",
            		"USE_EXT" => "Y"
            	)
            );?>
        <div class="footer-feedback">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:main.include",
            	".default",
            	Array(
            		"AREA_FILE_SHOW" => "file",
            		"AREA_FILE_SUFFIX" => "inc",
            		"COMPONENT_TEMPLATE" => ".default",
            		"EDIT_TEMPLATE" => "",
            		"PATH" => SITE_DIR."/include/foter_phone.php"
            	)
            );?>
            <?/*<a href="<?=SITE_DIR?>ajax/request_call.php" class="footer-order"><?=GetMessage('CALL_ORDER');?></a>*/?>
        </div>
        <div class="footer-copyrite">
            <span class="footer-copyritetext"><?$APPLICATION->IncludeComponent(
            	"bitrix:main.include",
            	".default",
            	Array(
            		"AREA_FILE_SHOW" => "file",
            		"AREA_FILE_SUFFIX" => "inc",
            		"COMPONENT_TEMPLATE" => ".default",
            		"EDIT_TEMPLATE" => "",
            		"PATH" => SITE_DIR."/include/copirite.php"
            	)
            );?></span>
            <div class="footer-social">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
	"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."/include/social.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
            </div>
            <a href="http://www.web-axioma.ru/" class="axi hover-white" target="_blank">made in Axi</a>
        </div>
    </div>
</div>
</div>
<div id="vverh"></div>
<a id="call-back" href="/ajax/request_call.php"><i></i></a>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'qWAE7U9TKp';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44600746 = new Ya.Metrika({
                    id:44600746,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44600746" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99037180-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>