<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="cooperation">
        <div class="cooperation-form width1015">
            <div class="cooperation-header">
                <h2><?=GetMessage('HEADER')?></h2>
                <?if(strlen($arResult["OK_MESSAGE"]) > 0){}else{?>
<span><?=GetMessage('HEADER_TEXT')?></span>
<?}?>
            </div>
            <div class="cooperation-telo">
            <?if(!empty($arResult["ERROR_MESSAGE"]))
                {
                	foreach($arResult["ERROR_MESSAGE"] as $v){
	   ?><span class="center"><?ShowError($v);?></span><?
	}
                }
                if(strlen($arResult["OK_MESSAGE"]) > 0)
                {
                	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div>
                    <?
                }else{?>
                <form action="<?=POST_FORM_ACTION_URI?>" method="POST">
                    <?=bitrix_sessid_post()?>
                    <input type="text" name="adress" class="adress" />
                    <input type="text" name="user_name" placeholder="<?=GetMessage('PLASE_NAME')?>" value="<?=$arResult["AUTHOR_NAME"]?>" />
                    <input class="maskinput" placeholder="<?=GetMessage('PLASE_PHONE')?>" type="text" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>"/>
                    <input class="cooperation-lastblock" type="text" name="user_email" placeholder="<?=GetMessage('PLASE_EMAIL')?>" value="<?=$arResult["AUTHOR_EMAIL"]?>" />
                    <textarea name="MESSAGE" placeholder="<?=GetMessage("COMMENT")?>"><?=$arResult["MESSAGE"]?></textarea>
                    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
                    <input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" />
                </form>
                <?}?>
            </div>
        </div>
    </div>
<script>
$('.maskinput').inputmask("+7(999) 999-99-99");
$('input[type="submit"]').click(function(){
        $(this).hide();
    });
</script>