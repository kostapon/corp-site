<?
$MESS ['MFT_NAME'] = "Your name";
$MESS ['MFT_EMAIL'] = "Your E-mail";
$MESS ['MFT_MESSAGE'] = "Message";
$MESS ['MFT_CAPTCHA'] = "Protection against automatic messages";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the word in the picture";
$MESS ['MFT_SUBMIT'] = "Start cooperation";
$MESS ['HEADER'] = "Do you want to cooperate with us?";
$MESS ['HEADER_TEXT'] = "Leave your contact details so that we can contact you.";
$MESS ['OK'] = "��";
$MESS ['PLASE_NAME'] = "Full name";
$MESS ['PLASE_PHONE'] = "+7(___) ___-__-__";
$MESS ['PLASE_EMAIL'] = "e-mail";
$MESS ['COMMENT'] = "Comment text";
?>