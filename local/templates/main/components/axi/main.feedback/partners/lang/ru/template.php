<?
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Начать сотрудничество";
$MESS ['HEADER'] = "Хотите сотрудничать с нами?";
$MESS ['HEADER_TEXT'] = "Оставьте свои контактные данные, чтобы мы могли с вами связаться";
$MESS ['OK'] = "Назад";
$MESS ['PLASE_NAME'] = "ФИО";
$MESS ['PLASE_PHONE'] = "+7(___) ___-__-__";
$MESS ['PLASE_EMAIL'] = "e-mail";
$MESS ['COMMENT'] = "Текст комментария";
?>