<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/fonts/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/style_callback.css" />
<div class="callback-body">
<button class="callback-close-btn"></button>
<h2><?=GetMessage('HEADER')?></h2>
<?if(strlen($arResult["OK_MESSAGE"]) > 0){}else{?>
<span><?=GetMessage('HEADER_TEXT')?></span>
<?}?>
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v){
	   ?><span class="center"><?ShowError($v);?></span><?
	}
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div>
    <button class="callback-ok"><?=GetMessage('OK')?></button>
    <?
}else{
?>
<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
<?=bitrix_sessid_post()?>
<input type="text" name="adress" class="adress" />
<input type="text" name="user_name" placeholder="<?=GetMessage('PLASE_NAME')?>" value="<?=$arResult["AUTHOR_NAME"]?>">
<input class="maskinput" placeholder="<?=GetMessage('PLASE_PHONE')?>" type="text" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>">
<input placeholder="<?=GetMessage('PLASE_EMAIL')?>" type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
<span class="callback-span-marginbot10"><?=GetMessage('PLASE_COMMENT')?></span>
<textarea name="MESSAGE"><?=$arResult["MESSAGE"]?></textarea>
<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>" onclick="yaCounter44600746.reachGoal('buy'); return true;">
</form>
<?}?>
</div>
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery/jquery-1.12.4.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/inputmask/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/inputmask/phone-codes/phone.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/inputmask/phone-codes/phone-be.js" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/inputmask/phone-codes/phone-ru.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
    $('.maskinput').inputmask("+7(999) 999-99-99");
    $('.callback-close-btn, .callback-ok').click(function(){
        window.parent.$('.fancybox-button--close').click();
    });
    $('input[type="submit"]').click(function(){
        $(this).hide();
    });
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter44600746 = new Ya.Metrika({
                    id:44600746,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/44600746" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->