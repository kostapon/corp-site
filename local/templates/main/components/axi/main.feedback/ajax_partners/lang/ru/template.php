<?
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
$MESS ['HEADER'] = "Хотите сотрудничать с нами?";
$MESS ['HEADER_TEXT'] = "Оставьте свои контактные данные, наш менеджер перезвонит вам в ближайшее время";
$MESS ['OK'] = "Назад";
$MESS ['PLASE_NAME'] = "Имя";
$MESS ['PLASE_PHONE'] = "+7(___) ___-__-__";
?>