<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="catalog-elements">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="text-decoration: none;">
    <div class="catalog-element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
    
        <?if($arItem["PROPERTIES"]['STOK']['VALUE']=='Показывать' && !empty($arItem["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE']) && !empty($arItem["DISPLAY_PROPERTIES"]['PRISE']['VALUE'])):?>
        <span class="catalog-prom">Акция <br><span class="catalog-prom-fontsize">-<?
            $prise1=$arItem["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE'];
            $prise2=$arItem["DISPLAY_PROPERTIES"]['PRISE']['VALUE'];
            $prise1=str_replace(',','.',$prise1);
            $prise2=str_replace(',','.',$prise2);
            $prom=round(100-$prise1/$prise2*100);
        echo $prom;
        ?>%</span></span>
        <?endif;?>
        
        <div class="catalog-img">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<span href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						/></span>
			<?else:?>
				<img
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					/>
			<?endif;?>
		<?else:?>
            <img src="<?=SITE_TEMPLATE_PATH?>/img/noimg.gif" />
        <?endif;?>
		</div>
        
        <div class="catalog-elementblock">
                    <span class="catalog-elementname">
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<span href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></span>
			<?else:?>
				<?echo $arItem["NAME"]?>
			<?endif;?>
		<?endif;?>
                </span>
        </div>
        <span class="catalog-elementdescription"><?if(!empty($arItem["DISPLAY_PROPERTIES"]['STOCK']['VALUE'])):?><?=$arItem["DISPLAY_PROPERTIES"]['STOCK']['VALUE']?><?else:?><span style="color: transparent;">_</span><?endif;?></span>
        <?
        $string=$arItem["DISPLAY_PROPERTIES"]['PRISE']['VALUE'];
        $i=strlen($string);
        $b=0;
        $result='';
        while($i>0){
            $i--;
            if($string[$i]!='.' && $string[$i]!=','){
                $b++;
            }else{
                $b=0;
            }
            $result.=$string[$i];
            if($b==3){$result.=' ';$b=0;}
        }
        $result=strrev($result);
        
        $string=$arItem["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE'];
        $i=strlen($string);
        $b=0;
        $result2='';
        while($i>0){
            $i--;
            if($string[$i]!='.' && $string[$i]!=','){
                $b++;
            }else{
                $b=0;
            }
            $result2.=$string[$i];
            if($b==3){$result2.=' ';$b=0;}
        }
        $result2=strrev($result2);
        ?>
        <?
        if($arItem["DISPLAY_PROPERTIES"]['PRISE_PRED']['VALUE']=='Конкретное значение'){
        ?>
         <span class="catalog-elementprise"> <?if(!empty($arItem["DISPLAY_PROPERTIES"]['PRISE']['VALUE'])):?><span 
            <?if($arItem["PROPERTIES"]['STOK']['VALUE']=='Показывать' && !empty($arItem["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE'])):?>
                class="catalog-elementprise-prom"
            <?endif;?>
         > <?=$result?> р.<?if(!empty($arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']))echo '/'.$arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE'];?></span> <?if(!empty($result2)){echo '<span class="catalog-elementprise-normal">'.$result2.' р.'; 
                    if(!empty($arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']))echo '/'.$arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']; 
            echo'</span>';}?><?else:?><span style="color: transparent;">_</span><?endif;?></span>
        <?}else{?>
        <span class="catalog-elementprise"> <?if(!empty($arItem["DISPLAY_PROPERTIES"]['PRISE']['VALUE'])):?><span
            <?if($arItem["PROPERTIES"]['STOK']['VALUE']=='Показывать' && !empty($arItem["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE'])):?>
                class="catalog-elementprise-prom"
            <?endif;?>
        >от <?=$result?> р.<?if(!empty($arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']))echo '/'.$arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE'];?></span> <?if(!empty($result2)){echo '<span class="catalog-elementprise-normal">от '.$result2.' р.';
                    if(!empty($arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']))echo '/'.$arItem["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE'];
            echo'</span>';}?><?else:?><span style="color: transparent;">_</span><?endif;?></span>
        <?}?>
       <span href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="catalog-more"><?=GetMessage('MORE');?></span>
        
	</div>
    </a>
<?endforeach;?>
</div>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>

