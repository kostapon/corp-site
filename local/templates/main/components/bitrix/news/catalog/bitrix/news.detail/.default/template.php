<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="elementslidermain">
        <div class="elementslider">
            <? 
            $i=0;
            if(count($arResult['DISPLAY_PROPERTIES']['GALLERY']['VALUE'])>1){
            foreach($arResult['DISPLAY_PROPERTIES']['GALLERY']['FILE_VALUE'] as $Item){
                $i++;
                
               $small_image=CFile::ResizeImageGet(CFile::GetFileArray($Item['ID']), Array("width"=>595, "height"=>400),BX_RESIZE_IMAGE_PROPORTIONAL);    
               $big_image=CFile::ResizeImageGet(CFile::GetFileArray($Item['ID']), Array("width"=>700, "height"=>600),BX_RESIZE_IMAGE_PROPORTIONAL);  
            ?>
            <div class="elementslider-img">
                <a data-fancybox="elementslider" href="<?=$big_image['src']?>">
                    <img src="<?=$small_image['src']?>" />
                    <span class="elementslider-plus"></span>
                </a>
            </div>
            <?}
            }else{
                if(is_array($arResult['DISPLAY_PROPERTIES']['GALLERY']['FILE_VALUE'])){
                $Item=$arResult['DISPLAY_PROPERTIES']['GALLERY']['FILE_VALUE'];
                    $i++;
                    
                   $small_image=CFile::ResizeImageGet(CFile::GetFileArray($Item['ID']), Array("width"=>595, "height"=>400),BX_RESIZE_IMAGE_PROPORTIONAL); 
                   $big_image=CFile::ResizeImageGet(CFile::GetFileArray($Item['ID']), Array("width"=>700, "height"=>600),BX_RESIZE_IMAGE_PROPORTIONAL);    
                ?>
                <div class="elementslider-img">
                    <a data-fancybox="elementslider" href="<?=$big_image['src']?>">
                        <img src="<?=$small_image['src']?>" />
                        <span class="elementslider-plus"></span>
                    </a>
                </div>
                <?
                }
            }
            if($i==0){
                ?>
                <div class="elementslider-img">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/noimg.gif" />
                </div>
                <?
            }
            ?>
        </div>
        <?if($arResult['DISPLAY_PROPERTIES']['STOK']['VALUE']=='Показывать' && !empty($arResult["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE']) && !empty($arResult["DISPLAY_PROPERTIES"]['PRISE']['VALUE'])){
                $prise1=$arResult["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE'];
                $prise2=$arResult["DISPLAY_PROPERTIES"]['PRISE']['VALUE'];
                $prise1=str_replace(',','.',$prise1);
                $prise2=str_replace(',','.',$prise2);
                $prom=round(100-$prise1/$prise2*100);
            ?>
            <span class="elementslider-prom">Акция<br /><span>-<?=$prom?>%</span></span>
        <?}?>
    </div>
    
    <div class="elemenparam">
        <?if(!empty($arResult['DISPLAY_PROPERTIES']['PRISE']['VALUE']) && $arResult['DISPLAY_PROPERTIES']['STOK']['VALUE']=='Показывать' && !empty($arResult['DISPLAY_PROPERTIES']['PRISE_DISK']['VALUE'])):?>
            <?
            $string=$arResult["DISPLAY_PROPERTIES"]['PRISE']['VALUE'];
            $i=strlen($string);
            $b=0;
            $result='';
            while($i>0){
                $i--;
                if($string[$i]!='.' && $string[$i]!=','){
                    $b++;
                }else{
                    $b=0;
                }
                $result.=$string[$i];
                if($b==3){$result.=' ';$b=0;}
            }
            $result=strrev($result);
            ?>
            <span class="elemenparam-prise"><i></i> <?if($arResult["DISPLAY_PROPERTIES"]['PRISE_PRED']['VALUE']!='Конкретное значение'){?>от<?}?> <?=$result?> руб.<?if(!empty($arResult["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']))echo '/'.$arResult["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE'];?></span>
        <?endif;?>
        
        <?if(!empty($arResult['DISPLAY_PROPERTIES']['PRISE']['VALUE'])):?>
        <?
        if($arResult['DISPLAY_PROPERTIES']['STOK']['VALUE']=='Показывать' && !empty($arResult["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE']) && !empty($arResult["DISPLAY_PROPERTIES"]['PRISE']['VALUE'])){
            $proc=$arResult['DISPLAY_PROPERTIES']['DISCOUNT']['VALUE'];
            $prise=$arResult['DISPLAY_PROPERTIES']['PRISE']['VALUE'];
            //$res=round($prise-$prise/100*$proc);
            $res=$arResult["DISPLAY_PROPERTIES"]['PRISE_DISK']['VALUE'];
        }else{
            $res=$arResult['DISPLAY_PROPERTIES']['PRISE']['VALUE'];
        }
        
        $string=strval($res);
            $i=strlen($string);
            $b=0;
            $result='';
            while($i>0){
                $i--;
                if($string[$i]!='.' && $string[$i]!=','){
                    $b++;
                }else{
                    $b=0;
                }
                $result.=$string[$i];
                if($b==3){$result.=' ';$b=0;}
            }
            $result=strrev($result);
        ?>
        <span class="<?if($arResult['DISPLAY_PROPERTIES']['STOK']['VALUE']=='Показывать' && !empty($arResult['DISPLAY_PROPERTIES']['DISCOUNT']['VALUE'])):?>elemenparam-prom<?else:?>elemenparam-prom elemenparam-prom-mod<?endif;?>"><i></i> <?if($arResult["DISPLAY_PROPERTIES"]['PRISE_PRED']['VALUE']!='Конкретное значение'){?>от<?}?> <span class="bold"><?=$result?></span> руб.
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE']))echo '/'.$arResult["DISPLAY_PROPERTIES"]['FORMAT_PRISE']['VALUE'];?></span>
        <?endif;?>
        
        <?if(!empty($arResult['DISPLAY_PROPERTIES']['STOCK']['VALUE'])):?>
        <span class="elemenparam-act"><i></i> <?=$arResult['DISPLAY_PROPERTIES']['STOCK']['VALUE']?></span>
        <?endif;?>
        <a class="elemenparam-buy" href="<?=SITE_DIR?>ajax/buy.php?id=<?=$arResult['ID']?>&id2=<?=$arResult['IBLOCK_ID']?>">Рассчитать заказ</a>
    </div>
    
    <?if( !empty($arResult['FIELDS']['PREVIEW_TEXT']) || !empty($arResult['FIELDS']['DETAIL_TEXT']) || !empty($arResult['PROPERTIES']['VIDEO']['~VALUE']['TEXT'])):?>
    <div class="elementdes">
        <div class="elementdes-header">
            <ul>
                <?if(!empty($arResult['FIELDS']['PREVIEW_TEXT'])){?>
                    <li data-id="des" class="active"><i class="elementdes-des"></i><?=GetMessage('DESCRIPTION')?></li>
                <?}?>
                <?if(!empty($arResult['FIELDS']['DETAIL_TEXT'])){?>
                    <li data-id="teh" <?if(empty($arResult['FIELDS']['PREVIEW_TEXT'])){?>class="active"<?}?>><i class="elementdes-teh"></i><?=GetMessage('SPECIFIKATIONS')?></li>
                <?}?>
                <?if(!empty($arResult['PROPERTIES']['VIDEO']['~VALUE']['TEXT'])){?>
                    <li data-id="vid" <?if(empty($arResult['FIELDS']['PREVIEW_TEXT'])&&empty($arResult['FIELDS']['DETAIL_TEXT'])){?>class="active"<?}?>><i class="elementdes-vid"></i><?=GetMessage('VIDEO')?></li>
                <?}?>
            </ul>
        </div>
        <div class="elementdes-telo">
            <?if(!empty($arResult['FIELDS']['PREVIEW_TEXT'])){?>
            <div data-block="des">
                <?=$arResult['FIELDS']['PREVIEW_TEXT']?>
            </div>
            <?}?>
            <?if(!empty($arResult['FIELDS']['DETAIL_TEXT'])){?>
            <div data-block="teh">
                <?=$arResult['FIELDS']['DETAIL_TEXT']?>
            </div>
            <?}?>
            <?if(!empty($arResult['PROPERTIES']['VIDEO']['~VALUE']['TEXT'])){?>
            <div data-block="vid">
                <?=$arResult['PROPERTIES']['VIDEO']['~VALUE']['TEXT']?>
            </div>
            <?}?>
        </div>
    </div>
    <?endif;?>