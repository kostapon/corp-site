<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(empty($arResult['DETAIL_TEXT'])){
    $arPage=CIBlockElement::GetList(Array("DATE_ACTIVE_FROM"=>"desc","name"=>'desc'),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
    $i=0;
    $b=0;
    while($arel=$arPage->GetNext()){
        $i++;
        if($arel['ID']==$arParams['ELEMENT_ID']){
            $b=$i;
        }
    }
    $i=floor($b/$arParams['NEWS_COUNT']);
    $i2=$b%$arParams['NEWS_COUNT'];
    if($i2>0)$i++;
    LocalRedirect('../?PAGEN_2='.$i);
}?>
<div class="landing">
        <div class="landing-head">
            <div class="landing-img">
                <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
            		<img
            			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
            			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
            			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
            			/>
            	<?else:?>
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/noimg.gif" />
                <?endif;?>
            </div>
            <div class="landing-header">
                <?if(!empty($arResult['DATE_ACTIVE_FROM'])&&!empty($arResult['DATE_ACTIVE_TO'])){
                    $date=explode(' ',$arResult['DATE_ACTIVE_FROM']);
                    $date1=$date[0];
                    $date=explode(' ',$arResult['DATE_ACTIVE_TO']);
                    $date2=$date[0];
                    ?>
                    <span class="landing-date"><?=$date1?> - <?=$date2?></span>
                <?}?>
                <h2 class="landing-name"><?=$arResult['NAME']?></h2>
                <p class="landing-description"><?=$arResult['PREVIEW_TEXT']?></p>
            </div>
        </div>
        <hr class="landing-line" />
        <div class="landing-telo">
            <?=$arResult['DETAIL_TEXT']?>
            <a class="landing-prev" onclick="history.back();"><?=GetMessage('PREV');?></a>
        </div>
    </div>