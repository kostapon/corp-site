<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	
    <div class="cell" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="cell-image">
            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
    			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
    			<?if(!empty($arItem['DETAIL_TEXT'])):?> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?else:?><span><?endif;?>	
                    <img
    						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
    						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
    						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
    						/>
                <?if(!empty($arItem['DETAIL_TEXT'])):?> </a><?else:?></span> <?endif;?>
    			<?else:?>
    				<img
    					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
    					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
    					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
    					/>
    			<?endif;?>
    		<?else:?>
                <img src="<?=SITE_TEMPLATE_PATH?>/img/noimg.gif" />
            <?endif;?>
        </div>
        <div class="cell-telo">
        <?if(!empty($arItem['DETAIL_TEXT'])):?>  <a class="cell-header" href="<?=$arItem["DETAIL_PAGE_URL"]?>"> <?else:?> <span class="cell-header cell-header-noline"> <?endif;?>  
                <h2><?=$arItem['NAME']?></h2>
        <?if(!empty($arItem['DETAIL_TEXT'])):?> </a> <?else:?> </span> <?endif;?>  
            <div class="cell-text"><?=$arItem['PROPERTIES']['DESCRIPTT']['~VALUE']['TEXT']?></div>
            <?if(!empty($arItem['PROPERTIES']['DATE']['VALUE'])){
                ?>
                <span class="cell-date"><?=$arItem['PROPERTIES']['DATE']['VALUE']?></span>
            <?}?>
           <?if(!empty($arItem['DETAIL_TEXT'])):?> <a class="cell-more" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('MORE');?></a> <?endif;?> 
        </div>
    </div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>