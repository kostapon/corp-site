<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="landing">
        <div class="landing-head">
            <div class="landing-img">
                <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
            		<img
            			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
            			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
            			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
            			/>
            	<?else:?>
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/noimg.gif" />
                <?endif;?>
            </div>
            <div class="landing-header">
                <?if(!empty($arResult['PROPERTIES']['DATE']['VALUE'])){
                    ?>
                    <span class="landing-date"><?=$arResult['PROPERTIES']['DATE']['VALUE']?></span>
                <?}?>
                <h2 class="landing-name"><?=$arResult['NAME']?></h2>
                <p class="landing-description"><?=$arResult['PREVIEW_TEXT']?></p>
            </div>
        </div>
        <hr class="landing-line" />
        <div class="landing-telo">
            <?=$arResult['DETAIL_TEXT']?>
            <a class="landing-prev" onclick="history.back();"><?=GetMessage('PREV');?></a>
        </div>
    </div>