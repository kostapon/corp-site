<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="catalog">
<?if (!empty($arResult)):?>

<div class="catalog-list">
            <ul>
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul><span>
                                        <span class='arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></span>
                                    </span></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="parent <?if ($arItem["SELECTED"]):?>activshow<?endif;?>"><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>active<?endif?>"><?=$arItem["TEXT"]?></a>
				<ul>
		<?else:?>
			<li class="parent"><a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?> class="active"<?endif?>><?=$arItem["TEXT"]?></a>
				<ul>
		<?endif?>

	<?else:?>

	

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>active<?endif?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="<?=$arItem["LINK"]?>" <?if ($arItem["SELECTED"]):?> class="active"<?endif?>><?=$arItem["TEXT"]?></a></li>
			<?endif?>

	

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul><span>
                                        <span class='arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></span>
                                    </span></li>", ($previousLevel-1) );?>
<?endif?>
</ul>
</div>

<?endif?>