<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $index;
?>
<?if (!empty($arResult)):?>
<ul class="footer-menu">
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li><a href="<?=$arItem["LINK"]?>" class="hover-white"><?=$arItem["TEXT"]?></a></li>
		<?endif?>

	<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="<?=$arItem["LINK"]?>" class="hover-white"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

	<?endif?>

<?endforeach?>
</ul>
<?endif?>
