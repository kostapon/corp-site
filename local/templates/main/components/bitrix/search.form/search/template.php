<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $index;
?>
<div class="header-search min-width1240">
                <form action="<?=$arResult["FORM_ACTION"]?>" class="<?if($index){echo 'width1240';}else{ if($GLOBALS['width1024']){echo 'width1240';}else{echo 'width1015';}}?>">
                    
                    <?if($arParams["USE_SUGGEST"] === "Y"):?><?$APPLICATION->IncludeComponent(
				"bitrix:search.suggest.input",
				"search",
				array(
					"NAME" => "q",
					"VALUE" => "",
					"INPUT_SIZE" => 15,
					"DROPDOWN_SIZE" => 10,
				),
				$component, array("HIDE_ICONS" => "Y")
			);?><?else:?><input type="text" name="q" value="" size="15" maxlength="50" class="search-text" placeholder="<?=GetMessage("PLASEHOLDER");?>" /><?endif;?>
                    
                    <input id="qqq2" class="search-submit" name="s" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
                </form>
                <script>
$('#qqq2').click(function(){
        var a=$('.search-text:eq(0)').val();
        var count=a.length;
        var i=-1;
        var s='';
        var d=0;
        if(a.charAt(0)=='0' || 
            a.charAt(0)=='1' || 
            a.charAt(0)=='2' ||
            a.charAt(0)=='3' ||
            a.charAt(0)=='4' ||
            a.charAt(0)=='5' ||
            a.charAt(0)=='6' ||
            a.charAt(0)=='7' ||
            a.charAt(0)=='8' ||
            a.charAt(0)=='9'){
                d=1;
            }
        while(i<count){
            i++;
            if( (a.charAt(i)=='0' || 
            a.charAt(i)=='1' || 
            a.charAt(i)=='2' ||
            a.charAt(i)=='3' ||
            a.charAt(i)=='4' ||
            a.charAt(i)=='5' ||
            a.charAt(i)=='6' ||
            a.charAt(i)=='7' ||
            a.charAt(i)=='8' ||
            a.charAt(i)=='9') && d==0 ){
                if(a.charAt(i-1)!=' ')
                    s=s+' ';
                d=1;
            }
            
            if( (a.charAt(i)!='0' && 
            a.charAt(i)!='1' && 
            a.charAt(i)!='2' &&
            a.charAt(i)!='3' &&
            a.charAt(i)!='4' &&
            a.charAt(i)!='5' &&
            a.charAt(i)!='6' &&
            a.charAt(i)!='7' &&
            a.charAt(i)!='8' &&
            a.charAt(i)!='9') && d==1 ){
                if(a.charAt(i)!=' ')
                    s=s+' ';
                d=0;
            }
            
            s=s+a.charAt(i);
        }
        $('.search-text:eq(0)').val(s);
    });
</script>
</div>