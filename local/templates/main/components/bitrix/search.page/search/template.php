<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="search-page">
<form action="" method="get">
<?if($arParams["USE_SUGGEST"] === "Y"):
	if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
	{
		$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
		$obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
		$obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
	}
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:search.suggest.input",
		"",
		array(
			"NAME" => "q",
			"VALUE" => $arResult["REQUEST"]["~QUERY"],
			"INPUT_SIZE" => 40,
			"DROPDOWN_SIZE" => 10,
			"FILTER_MD5" => $arResult["FILTER_MD5"],
		),
		$component, array("HIDE_ICONS" => "Y")
	);?>
<?else:?>
	<input id="qq" type="text" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" size="40" />
<?endif;?>
<?if($arParams["SHOW_WHERE"]):?>
	&nbsp;<select name="where">
	<option value=""><?=GetMessage("SEARCH_ALL")?></option>
	<?foreach($arResult["DROPDOWN"] as $key=>$value):?>
	<option value="<?=$key?>"<?if($arResult["REQUEST"]["WHERE"]==$key) echo " selected"?>><?=$value?></option>
	<?endforeach?>
	</select>
<?endif;?>
	&nbsp;<input id="qqq" type="submit" value="<?=GetMessage("SEARCH_GO")?>" />
	<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
<?if($arParams["SHOW_WHEN"]):?>
	<script>
	var switch_search_params = function()
	{
		var sp = document.getElementById('search_params');
		var flag;
		var i;

		if(sp.style.display == 'none')
		{
			flag = false;
			sp.style.display = 'block'
		}
		else
		{
			flag = true;
			sp.style.display = 'none';
		}

		var from = document.getElementsByName('from');
		for(i = 0; i < from.length; i++)
			if(from[i].type.toLowerCase() == 'text')
				from[i].disabled = flag;

		var to = document.getElementsByName('to');
		for(i = 0; i < to.length; i++)
			if(to[i].type.toLowerCase() == 'text')
				to[i].disabled = flag;

		return false;
	}
	</script>
	<br /><a class="search-page-params" href="#" onclick="return switch_search_params()"><?echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')?></a>
	<div id="search_params" class="search-page-params" style="display:<?echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'?>">
		<?$APPLICATION->IncludeComponent(
			'bitrix:main.calendar',
			'',
			array(
				'SHOW_INPUT' => 'Y',
				'INPUT_NAME' => 'from',
				'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
				'INPUT_NAME_FINISH' => 'to',
				'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
				'INPUT_ADDITIONAL_ATTR' => 'size="10"',
			),
			null,
			array('HIDE_ICONS' => 'Y')
		);?>
	</div>
<?endif?>
</form><br />
<script>
$('#qqq').click(function(){
        var a=$('.search-suggest:eq(0)').val();
        var count=a.length;
        var i=-1;
        var s='';
        var d=0;
        if(a.charAt(0)=='0' || 
            a.charAt(0)=='1' || 
            a.charAt(0)=='2' ||
            a.charAt(0)=='3' ||
            a.charAt(0)=='4' ||
            a.charAt(0)=='5' ||
            a.charAt(0)=='6' ||
            a.charAt(0)=='7' ||
            a.charAt(0)=='8' ||
            a.charAt(0)=='9'){
                d=1;
            }
        while(i<count){
            i++;
            if( (a.charAt(i)=='0' || 
            a.charAt(i)=='1' || 
            a.charAt(i)=='2' ||
            a.charAt(i)=='3' ||
            a.charAt(i)=='4' ||
            a.charAt(i)=='5' ||
            a.charAt(i)=='6' ||
            a.charAt(i)=='7' ||
            a.charAt(i)=='8' ||
            a.charAt(i)=='9') && d==0 ){
                if(a.charAt(i-1)!=' ')
                    s=s+' ';
                d=1;
            }
            
            if( (a.charAt(i)!='0' && 
            a.charAt(i)!='1' && 
            a.charAt(i)!='2' &&
            a.charAt(i)!='3' &&
            a.charAt(i)!='4' &&
            a.charAt(i)!='5' &&
            a.charAt(i)!='6' &&
            a.charAt(i)!='7' &&
            a.charAt(i)!='8' &&
            a.charAt(i)!='9') && d==1 ){
                if(a.charAt(i)!=' ')
                    s=s+' ';
                d=0;
            }
            
            s=s+a.charAt(i);
        }
        $('.search-suggest:eq(0)').val(s);
    });
</script>
<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>

<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
<?elseif($arResult["ERROR_CODE"]!=0):?>
	<p><?=GetMessage("SEARCH_ERROR")?></p>
	<?ShowError($arResult["ERROR_TEXT"]);?>
	<p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
	<br /><br />
	<p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
	<table border="0" cellpadding="5">
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
			<td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
			<td><?=GetMessage("SEARCH_AND_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
			<td><?=GetMessage("SEARCH_OR_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
			<td><?=GetMessage("SEARCH_NOT_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top">( )</td>
			<td valign="top">&nbsp;</td>
			<td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
		</tr>
	</table>
<?elseif(count($arResult["SEARCH"])>0):?>
	<?if($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
	<br />
	<?foreach($arResult["SEARCH"] as $arItem):?>
    <?
        $image='';
        if($arItem['MODULE_ID']=='iblock'){
            $idblock=$arItem['PARAM2'];
            $idelem=$arItem['ITEM_ID'];
            $res=CIBlockElement::GetByID($idelem);
            $ar_res = $res->GetNext();
                if(!empty($ar_res['PREVIEW_PICTURE'])){
                    $image=CFile::GetPath($ar_res['PREVIEW_PICTURE']);
                }
        }
        $prise='';//изначальная цена
        $prise_disckont='';//цена со скидкой
        $disckont='';//выводить акцию
        $prise_pred='';//Представление цены(от не от)
        $prise_format='';//Цена за (кг., м. и т.д.)
        $arproperty=CIBlockElement::GetProperty($idblock,$idelem);
        while($arRes=$arproperty->Fetch()){
            if($arRes['CODE']=='PRISE') $prise=$arRes['VALUE'];//изначальная цена
            if($arRes['CODE']=='PRISE_DISK') $prise_disckont=$arRes['VALUE'];//цена со скидкой
            if($arRes['CODE']=='STOK') $disckont=$arRes['VALUE_ENUM'];//выводить акцию
            if($arRes['CODE']=='PRISE_PRED') $prise_pred=$arRes['VALUE_ENUM'];//Представление цены(от не от)
            if($arRes['CODE']=='FORMAT_PRISE') $prise_format=$arRes['VALUE'];//Цена за (кг., м. и т.д.)
        }
        if($prise_pred!='От')$prise_pred='';
        if(!empty($prise) && !empty($prise_disckont) && $disckont=='Показывать'){
            $prise=$prise_pred.' '.$prise_disckont.' р.';
            if(!empty($prise_format))$prise.='/'.$prise_format;
        }else if(!empty($prise)){
            $prise=$prise_pred.' '.$prise.' р.';
            if(!empty($prise_format))$prise.='/'.$prise_format;
        }
    ?>
<div class="cell cell-search" <?if(empty($arItem["BODY_FORMATED"])):?>style="height: 200px;"<?endif;?>>
        <?if(!empty($arItem["BODY_FORMATED"])&&!empty($image)):?>
        <div class="cell-image">
            <a href="<?echo $arItem["URL"]?>">	
                <img src="<?=$image?>">
            </a>
        </div>
        <?endif;?>
        <div class="cell-telo" <?if(!empty($arItem["BODY_FORMATED"])&&!empty($image)){?>style="width:800px;"<?}?>>
          <a class="cell-header" href="<?echo $arItem["URL"]?>" <?if(!empty($arItem["BODY_FORMATED"])&&!empty($image)){?>style="max-width:650px;"<?}?>>   
                <h2><?echo $arItem["TITLE_FORMATED"]?></h2>
         </a>   
            <div class="cell-text" <?if(!empty($arItem["BODY_FORMATED"])&&!empty($image)){?>style="width:650px;"<?}?>><p>
            <?echo $arItem["BODY_FORMATED"]?>
</p></div>
                        <?if(!empty($prise)){?><span class="cell-date cell-date-bold"><?=$prise?></span><?}?>
                        <a class="cell-more" href="<?echo $arItem["URL"]?>"><?=GetMessage('MORE');?></a>  
        </div>
    </div>

	<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
	<br />
	<p>
	<?if($arResult["REQUEST"]["HOW"]=="d"):?>
		<a href="<?=$arResult["URL"]?>&amp;how=r<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"><?=GetMessage("SEARCH_SORT_BY_RANK")?></a>&nbsp;|&nbsp;<b><?=GetMessage("SEARCH_SORTED_BY_DATE")?></b>
	<?else:?>
		<b><?=GetMessage("SEARCH_SORTED_BY_RANK")?></b>&nbsp;|&nbsp;<a href="<?=$arResult["URL"]?>&amp;how=d<?echo $arResult["REQUEST"]["FROM"]? '&amp;from='.$arResult["REQUEST"]["FROM"]: ''?><?echo $arResult["REQUEST"]["TO"]? '&amp;to='.$arResult["REQUEST"]["TO"]: ''?>"><?=GetMessage("SEARCH_SORT_BY_DATE")?></a>
	<?endif;?>
	</p>
<?else:?>
	<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
<?endif;?>
</div>