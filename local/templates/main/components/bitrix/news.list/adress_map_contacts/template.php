<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
global $APPLICATION;
          try { 
             $geo = new Geo(); // � ���� �� ��������� ��������� windows-1251, �� ���� ������
             $value = $geo->get_value('city'); // �������� geo.php ��� ������ ��������
             if (!empty($value)) 
               $city = mb_convert_encoding($value, "utf-8", "windows-1251"); // ���� �� ��������� �������� �����
          } catch (HttpException $exception) {}
$b=0;
if($SITY = $APPLICATION->get_cookie("SITY")){
    $i=0;
    foreach($arResult["ITEMS"] as $arItem){
        if($arItem["NAME"]==$SITY){
            $b=$i;
        }
        $i++;
    }
}else{
    $i=0;
    foreach($arResult["ITEMS"] as $arItem){
        if($arItem["NAME"]==$city){
            $b=$i;
            $APPLICATION->set_cookie("SITY",$city);
        }
        $i++;
    }
}

?>
<div class="map map-contacts min-width1240">
        <div id="map"></div>
        <script>
        var metki =[
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    '<?echo $arItem['NAME'].' '.$arItem["DISPLAY_PROPERTIES"]['ADRESS']['VALUE']?>',
                <?endforeach;?>
             ];
             
        var active_metki=<?=$b?>;
        function initMap() {
            
            $.get('<?=SITE_TEMPLATE_PATH?>/js/map.js');
            
            var resultmetki=[];
         
             metki.forEach(function(item){
                resultmetki.push(item.replace(/ /g,"%20"));
             });
            
            resultmetki.forEach(function(item,i){
            $.ajax({
              url: 'http://maps.googleapis.com/maps/api/geocode/json?address='+item+'&sensor=false&language=ru',
              dataType: 'json',
              success: function(data){
                var lat=data['results']['0']['geometry']['location']['lat'];
                var lng=data['results']['0']['geometry']['location']['lng'];
                var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, lng),
                        icon: '<?=SITE_TEMPLATE_PATH?>/img/iconmap.png',
                        map: map
                      });
                resultmetki[i]=[];
                resultmetki[i]['lat']=lat;
                resultmetki[i]['lng']=lng; 
                if(i==active_metki){
                    map.setCenter({lat:resultmetki[i]['lat'], lng:resultmetki[i]['lng']});
                }
              }
            });
         });
            
            $('.contactblock-sities li').click(function(){
                var ind=$(this).attr('data-map-metka');
                map.setCenter({lat:resultmetki[ind]['lat'], lng:resultmetki[ind]['lng']});
            });
            
          }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCX7WXynThLBvP4AYCOizv1cnuPBGJBPbw&callback=initMap" async defer></script>
        <div class="map-contactblock">
            <div class="contactblock-adressa contactblock-adressa-map">
                    <?$i=0;
                    foreach($arResult["ITEMS"] as $arItem):
                    if($i==$b){
                    ?>
                        <span class="contactblock-sity"><?echo $arItem["NAME"]?></span>
                        <?$adres=$arItem["DISPLAY_PROPERTIES"]['ADRESS']['VALUE'];
                          $phone=$arItem["DISPLAY_PROPERTIES"]['PHONE']['VALUE'];
                          $value_phone=$arItem["DISPLAY_PROPERTIES"]['VALUE_PHONE']['VALUE'];
                        ?>
                    <?}
                    $i++;
                    endforeach;?>
                    <ul class="contactblock-sities" style="display: none;">
                        <?$i=0;
                        foreach($arResult["ITEMS"] as $arItem):?>
                            <li data-adres="<?echo $arItem["DISPLAY_PROPERTIES"]['ADRESS']['VALUE']?>" data-phone="<?echo $arItem["DISPLAY_PROPERTIES"]['PHONE']['VALUE']?>" data-phone-value="<?echo $arItem["DISPLAY_PROPERTIES"]['VALUE_PHONE']['VALUE']?>" data-map-metka="<?=$i;?>"><span><?echo $arItem["NAME"]?></span></li>
                        <?
                        $i++;
                        endforeach;?>
                    </ul>
                    <br>
                    <span class="contactblock-adres"><?=$adres;?></span>
                    <br>
                    <a class="contactblock-phone hover phone-sity" href="tel:<?=$value_phone?>"><?=$phone?></a>
                    <br>
                    <?$APPLICATION->IncludeComponent(
                    	"bitrix:main.include",
                    	".default",
                    	Array(
                    		"AREA_FILE_SHOW" => "file",
                    		"AREA_FILE_SUFFIX" => "inc",
                    		"COMPONENT_TEMPLATE" => ".default",
                    		"EDIT_TEMPLATE" => "",
                    		"PATH" => SITE_DIR."/include/email.php"
                    	)
                    );?>
            </div>
        </div>
    </div>