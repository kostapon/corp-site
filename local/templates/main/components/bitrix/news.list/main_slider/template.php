<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="slider min-width1240">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div style="height: 727px;" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="slider-block" <?if(is_array($arItem["PREVIEW_PICTURE"])){?>style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);"<?}?> >
            <div class="slider-img" style="<?if(is_array($arItem["DETAIL_PICTURE"])){?>background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);<?}?>"></div>
                <div class="slider-content width1240">
                    <h2 class="slider-header"><?=$arItem["NAME"]?></h2>
                    <p class="slider-description"><?=$arItem["PREVIEW_TEXT"]?></p>
                    <?if(!empty($arItem["PROPERTIES"]["LINK"]["VALUE"]) && $arItem["PROPERTIES"]["LINK_BUTTOM"]["VALUE"]=='Показывать'){?>
                    <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" class="slider-more"><?=GetMessage("MORE");?></a>
                    <?}?>
                    <?if(!empty($arItem["PROPERTIES"]["PROM"]["VALUE"]) && $arItem["PROPERTIES"]["PROM_BUTTOM"]["VALUE"]=='Показывать'){?>
                    <span class="slider-discount"><?=$arItem["PROPERTIES"]["PROM"]["VALUE"]?></span>
                    <?}?>
                </div>
	   </div>
    </div>
<?endforeach;?>
</section>