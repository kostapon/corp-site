<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="partners-block">
            <h2 class="partners-header"><?=GetMessage('TITLE_PARTNERS');?></h2>
            <div class="partners-slider">
<?
$i=0;
?>
<?foreach($arResult["ITEMS"] as $arItem):
if($arItem['PROPERTIES']['DISPLAY']['VALUE']=='Показывать' && (is_array($arItem["PREVIEW_PICTURE"]) || is_array($arItem["DETAIL_PICTURE"]) )):
?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <?
    $i++;
    if($i==7){
        $i=1;
    }
    ?>
    <?if($i==1){?><div><?}?>
<?if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])){?><a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>"><?}?>
<div class="partners-element partners-element-hover" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
        <?
            $img=CFile::ResizeImageGet(CFile::GetFileArray($arItem["PREVIEW_PICTURE"]['ID']),Array("width"=>150, "height"=>90),BX_RESIZE_IMAGE_PROPORTIONAL);
        ?>
				<img
					src="<?=$img["src"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					/>
                <?/*if(!is_array($arItem["DETAIL_PICTURE"])){?>
                    <img
					src="<?=$img["src"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					/>
                <?}*/?>
		<?endif?>
        <?/*if(is_array($arItem["DETAIL_PICTURE"])):?>
				<img
					src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
					alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
					title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
					/>
                    <?if(!is_array($arItem["PREVIEW_PICTURE"])){?>
                        <img
    					src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
    					alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
    					title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
    					/>
                    <?}?>
		<?endif*/?>
</div>	
<?if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])){?></a><?}?>	
        
     <?if($i==6){?></div><?}?>   
<?
endif;
endforeach;?>
<?if($i<6){?></div><?}?>
</div>
<?$APPLICATION->IncludeComponent(
                	"bitrix:main.include",
                	".default",
                	Array(
                		"AREA_FILE_SHOW" => "file",
                		"AREA_FILE_SUFFIX" => "inc",
                		"COMPONENT_TEMPLATE" => ".default",
                		"EDIT_TEMPLATE" => "",
                		"PATH" => SITE_DIR."/include/link_partners.php"
                	)
                );?>
</div>