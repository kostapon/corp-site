<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="sliderobertka">
    <div class="sliderpartners">
<?$i=0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
    $i++;
    if($i==9){
        $i=1;
    }
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <?if($i==1):?><div class="sliderpartners-slide"><?endif;?>

    <div class="sliderpartners-element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
<?if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])){?><a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" style="text-decoration: none;"><?}?>
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
				<img
					src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
					alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
					title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
					/>
		<?else:?>
            <img src="<?=SITE_TEMPLATE_PATH?>/img/noimg2.gif" />
        <?endif;?>
<?if(!empty($arItem['PROPERTIES']['LINK']['VALUE'])){?></a><?}?>
</div>

	   <?if($i==8):?></div><?endif;?>
<?endforeach;?>
    <?if($i<8):?></div><?endif;?>
</div>
</div>
