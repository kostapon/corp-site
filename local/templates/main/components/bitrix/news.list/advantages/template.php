<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="advantages min-width1240">
    <div class="advantages-content">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="advantages-element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <?if(is_array($arItem["PREVIEW_PICTURE"])){?>
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="advantages-img" />
        <?}?>
        <div class="advantages-name-bottom">
                <span class="advantages-name"><?=$arItem["NAME"]?></span>
            </div>
        </div>
<?endforeach;?>
</div>
</div>
