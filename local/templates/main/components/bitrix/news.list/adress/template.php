<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
global $APPLICATION;
          try { 
             $geo = new Geo(); // у него по умолчанию кодировка windows-1251, но есть конфиг
             $value = $geo->get_value('city'); // смотрите geo.php для других значений
             if (!empty($value)) 
               $city = mb_convert_encoding($value, "utf-8", "windows-1251"); // если по умолчанию получает город
          } catch (HttpException $exception) {}
$b=0;
if($SITY = $APPLICATION->get_cookie("SITY")){
    $i=0;
    foreach($arResult["ITEMS"] as $arItem){
        if($arItem["NAME"]==$SITY){
            $b=$i;
        }
        $i++;
    }
}else{
    $i=0;
    foreach($arResult["ITEMS"] as $arItem){
        if($arItem["NAME"]==$city){
            $b=$i;
            $APPLICATION->set_cookie("SITY",$city);
        }
        $i++;
    }
   
}
?>
<div class="contactblock-adressa">
<?$i=0;
foreach($arResult["ITEMS"] as $arItem):
if($i==$b){
?>
    <span class="contactblock-sity"><?echo $arItem["NAME"]?></span>
    <?$adres=$arItem["DISPLAY_PROPERTIES"]['ADRESS']['VALUE'];
      $phone=$arItem["DISPLAY_PROPERTIES"]['PHONE']['VALUE'];
      $value_phone=$arItem["DISPLAY_PROPERTIES"]['VALUE_PHONE']['VALUE'];
    ?>
<?}
$i++;
endforeach;?>
<ul class="contactblock-sities">
<?$i=0;
foreach($arResult["ITEMS"] as $arItem):?>
    <li data-adres="<?echo $arItem["DISPLAY_PROPERTIES"]['ADRESS']['VALUE']?>" data-phone="<?echo $arItem["DISPLAY_PROPERTIES"]['PHONE']['VALUE']?>" data-phone-value="<?echo $arItem["DISPLAY_PROPERTIES"]['VALUE_PHONE']['VALUE']?>" data-map-metka="<?=$i;?>"><span><?echo $arItem["NAME"]?></span></li>
<?
$i++;
endforeach;?>
</ul>
<br />
<span class="contactblock-adres"><?=$adres;?></span>
</div>
            <div class="contactblock-phons">
                <span class="contactblock-des"><?=GetMessage('CALL_DESCRIPT');?></span>
                <?$APPLICATION->IncludeComponent(
                	"bitrix:main.include",
                	".default",
                	Array(
                		"AREA_FILE_SHOW" => "file",
                		"AREA_FILE_SUFFIX" => "inc",
                		"COMPONENT_TEMPLATE" => ".default",
                		"EDIT_TEMPLATE" => "",
                		"PATH" => SITE_DIR."/include/main_phone.php"
                	)
                );?>
                <i class="fa fa-phone" aria-hidden="true"></i><a href="tel:<?=$value_phone?>" class="contactblock-phone phone-sity hover"><?=$phone?></a>
                <a href="<?=SITE_DIR?>ajax/request_call.php" class="contactblock-collback"><?=GetMessage('CALL_ORDER');?></a>
           </div>