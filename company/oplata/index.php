<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Промышленное снабжение - Оплата");
$APPLICATION->SetTitle("Оплата");
?><p>
	 Оплатить заказ вы можете удобным для себя способом. Мы работаем как с юридическими лицами, так и с физическими.
</p>
<h2>
Варианты оплаты </h2>
<p>
</p>
<table class="tablebeauty">
<tbody>
<tr>
	<td>
 <img width="75" alt="Оплата Юр.лицам и ИП" src="/upload/medialibrary/6de/6de7a131118400cac53b743de0762088.png" height="75"><br>
	</td>
	<td>
		<p style="text-align: left;">
 <b>Оплата для юридических лиц и ИП</b>
		</p>
		<p style="text-align: left;">
			 Получить счет на оплату можно, выслав реквизиты вместе с заявкой на почту. Бухгалтерские документы при самовывозе вы получите вместе с товаром. <br>
		</p>
		<p style="text-align: left;">
		</p>
		<p style="text-align: left;">
			 Если доставку товара организуем мы, документы будут отправлены вместе с грузом или по почте.
		</p>
	</td>
</tr>
<tr>
	<td>
 <img width="75" alt="Оплата физ.лицами" src="/upload/medialibrary/2dc/2dc3d65538192a657e87ed0f84470ebf.png" height="75"><br>
	</td>
	<td>
		<p style="text-align: left;">
 <b>Оплата для физических лиц</b>
		</p>
		<p style="text-align: left;">
			 Сообщите при заказе ваш город и мы предложим удобные для вас варианты оплаты.
		</p>
	</td>
</tr>
</tbody>
</table>
 <br>
<p>
</p>
<h2><br>
 </h2><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>